import { Movable } from "./Movable";

export interface GameObject extends Movable{
    
    name:string;
}