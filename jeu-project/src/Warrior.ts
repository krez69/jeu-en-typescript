
import { GameMap } from "./GameMap";
import { GameObject } from "./GameObject";
import { Unit } from "./Unit";

export class Warrior implements Unit {

  protected selected = false;

  name: string;
  hp: number;
  mana: number;
  pointAction: number;
  canMove: boolean = true;
  canAction: boolean = true;
  currentX: number;
  currentY: number;
  

  constructor(name: string, hp: number, mana: number, pointAction: number, canMove: boolean, canAction: boolean, currentX: number, currentY: number) {
    this.name = name;
    this.hp = hp;
    this.mana = mana;
    this.pointAction = pointAction;
    this.canMove = canMove;
    this.canAction = canAction;
    this.currentX = currentX;
    this.currentY = currentY;
  }

  move(x: number, y: number) {
    if (this.selected) {
      // Vérifiez si la case cible est valide
      if (x > 0 && x < GameMap.size && y > 0 && y < GameMap.size) {
        // Vérifiez si la case cible est libre
        if (GameMap.grid[x][y] === null) {
          // Déplacez le Warrior
          GameMap.grid[x][y] = this;
          GameMap.grid[this.currentX][this.currentY] = null;
          this.currentX = x;
          this.currentY = y;
          console.log(`Le ${this.name} a été déplacé à la position (${x}, ${y}).`);
        } else {
          console.log(`La case (${x}, ${y}) est occupée.`);
        }
      } else {
        console.log(`La case (${x}, ${y}) est en dehors des limites de la carte.`);
      }
    } else {
      console.log(`Le ${this.name} n'est pas sélectionné. Sélectionnez d'abord le Warrior.`);
    }
  }

  select() {
    this.selected = true;
    console.log(`Le ${this.name} est sélectionné.`);
  }

  deselect() {
    this.selected = false;
    console.log(`Le ${this.name} n'est pas ou plus sélectionné`)
  }

  performAction(_target: Unit | GameObject | null) {
    // Implémentez ici les actions possibles pour le Warrior (attaque, action spéciale, etc.)
  }


}