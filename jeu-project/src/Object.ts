import { GameObject } from "./GameObject";

export class Object implements GameObject{
    name: string;

    constructor(name:string){
        this.name = name;
    }

    move(x: number, y: number): void {
        throw new Error("Method not implemented.");
    }

}