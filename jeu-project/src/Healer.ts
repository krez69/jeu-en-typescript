import { GameObject } from './GameObject';
import { Unit } from './Unit';

export class Healer implements Unit{
    name: string;
    hp: number;
    mana: number;
    pointAction: number;
    canMove: boolean = true;
    canAction: boolean = true;
    currentX: number; 
    currentY: number;

    constructor(name:string){
        this.name = name;
        this.hp = 100;       
        this.mana = 0;
        this.pointAction = 15;
        this.currentX = 0;
        this.currentY = 0;

    }

    move(_x: number, _y: number) {
        // Implémentez ici la logique pour déplacer le Warrior à la position (x, y) sur la carte
      }
    
    select() {
        // Implémentez ici la logique pour sélectionner le Warrior et afficher ses options d'action
      }
    
    deselect() {
        // Implémentez ici la logique pour désélectionner le Warrior
      }
    
    performAction(_target: Unit | GameObject | null) {
        // Implémentez ici les actions possibles pour le Warrior (attaque, action spéciale, etc.)
      }
    
}