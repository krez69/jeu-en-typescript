import { GameObject } from "./GameObject";
import { Unit } from "./Unit";

//generate ramdon position
// function genRandomNumber(range: number){
//     return Math.floor(Math.random() * range)
//   }

//   let RandomPosition = function(this: any, range: number) {
//     this.x=genRandomNumber(range);
//     this.y=genRandomNumber(range);
//   }

export class GameMap {
  
  static grid: any;
  static size: number;

  grid: (Unit | GameObject | null)[][] = [];

  constructor(size: any) {
    GameMap.size = size;
    GameMap.grid = new Array(size).fill(null).map(() => new Array(size).fill(null));
  }


  isCellOccupied(unitToPlace: Unit, x: number, y: number): boolean {
    const unitAtCell = GameMap.grid[x][y];
    return unitAtCell !== null && unitAtCell !== unitToPlace;
  }

  isCellOccupiedByObject(unitToObject: GameObject, x: number, y: number): boolean {
    const unitAtCell = GameMap.grid[x][y];
    return unitAtCell !== null && unitAtCell !== unitToObject;
  }


  placeUnit(unit: Unit, x: number, y: number) {
    // Place une unité à une position donnée sur la carte
    if (!this.isCellOccupied(unit, x, y)) {
      GameMap.grid[x][y] = unit;
    } else {
      // La case est occupée par une unité différente ou la même unité, affichez un message d'erreur ou effectuez une autre action
      console.log("La case est déjà occupée par une unité. Impossible de placer l'unité ici.");
    }
  }

  placeObject(obj: GameObject, x: number, y: number) {
    // Place un objet à une position donnée sur la carte
    if (!this.isCellOccupiedByObject(obj, x, y)) {
      GameMap.grid[x][y] = obj;
    } else {
      // La case est occupée par une unité différente ou la même unité, affichez un message d'erreur ou effectuez une autre action
      console.log("La case est déjà occupée par un objet. Impossible de placer l'objet ici.");
    }
  }

}


