import { GameObject } from "./GameObject";
import { Movable } from "./Movable";


// Interface pour représenter une unité qui peut effectuer des actions
export interface Unit {
    
    name:string;
    hp:number;
    mana:number;
    pointAction:number;
    canMove: boolean;
    canAction : boolean;
    currentX: number;
    currentY: number;

    select():void;

    deselect():void;

    performAction(target: Unit | GameObject | null): void; // Effectue une action (attaque, action spéciale, etc.)

}